#Design description and rationale

## Key constraints and assumptions.

I chose to implement a library catalogue system as a demonstration of a working OO
design.  The result is a RESTful API that supports key library operations - borrowing and returning books, placing and cancelling holds, looking at a borrower's current status, and most importantly searching for books.

It was originally intended to act as a server to the partly implemented Flutter client provided
in the other repository, but unfortunately this did not happen.  I hoped to connect the Flutter
application up but it turns out that for reasons that I could not determine, what appears to be 
the exact same request made through Postman worked fine, but when made through Flutter's http
request system caused exceptions on the Java side.  Hopefully you should be able to get some 
sense of how they would have hooked up together.

In the time available, it was obviously infeasible to implement a production-quality 
library cataloguing system, and many, many aspects of a real production system were not implemented,
and many quality attributes were ignored.  Some particularly notable ones include:

* There was no attempt whatsoever to implement any security controls.
* No attempt was made to optimize for performance.
* There are no unit tests, and no automated system tests.
* The logic of borrwing and holds omits a number of complexities that would be necessary
  in a real library (for instance, books don't immediately go back on the shelf when returned).
* Despite a design that was originally intended to support different types of items 
  in the library, which would be searchable on the variety of attributes that each invidiaul
  item type would have, this was not implemented.
  
Nevertheless, I hope there is sufficient meat present in the application to demonstrate
that I have some ability to design and implement OO applications.

##Architecture

This is a RESTful API for a library catalogue system put together for demonstration 
purposes. 

The application was put together in Java using the combination of:

* Spring Boot
* Hibernate
* H2 (an in-memory SQL database).

Spring Boot/Hibernate were chosen as, to my knowledge, by far the most popular way to 
implement such SQL database driven RESTFul APIs in Java.  This is the first application
I have written using this particular toolchain (and it shows).

H2 was chosen as a convenient zero-configuration SQL database to use for testing/demo
purposes.  Obviously, it would not be suitable for an actual production system.

## Problem domain description 

The basis for my model was that of a large university library.  In such a library, there
are typically multiple *collections*.  The distinguishing logical feature of a collection
is that there are specific borrowing rights attached to all the items in that collection.

Within a collection, there are a large number of copies of books.  Often, there may be 
multiple, identical copies of the same book within a collection, and there may also be 
copies of the same book located in multiple collections - for instance, a class textbook 
may have a couple of copies in a "not for borrowing" collection, a few more in a "short term loan" 
collection, and yet more in the main library collection.

To allow fair, convenient access to in-demand books, libraries will allow borrowers to place holds
on items.  An available book on the shelf, if a hold is placed on it, is transferred to a hold shelf (for a limited time, though this is not modelled in my solution) to allow a borrower to pick it up.  
If no copies of the book are available at that time, the borrower's hold request is placed in a queue
and is satisfied when an item becomes available when they are at the head of the hold queue for that
book.

As far as borrowing goes, borrowers can borrow copies of book under the rules that apply to that
collection, returning them (hopefully) before the due date.

## Data model

The data model, as implemented as entity classes that Hibernate turned into database classes, is
summarised in the rough UML diagram in this directory.

There is a reasonable correspondence between concepts identified in the problem domain description 
and the resulting set of entity classes:

* _LibraryCollection_: The library collection with its associated items and lending rights
* _Book_: Representing a catalogue record for a book, containing information about title, author etc.
* _ItemCopy_: Representing an individual copy of a book.
* _Borrower_: A person who borrows books from the library.
* _Borrowing_: A record of the action of a borrower borrowing an ItemCopy from the library.
* _ActiveHold_: A record of a book being placed on the "hold shelf" for a borrower. 
* _PendingHold_: A hold placed on a book which is waiting to be fulfilled (made active) when 
a book becomes available.

The key addition to the concepts described above was the  "CollectionSet", which is 
associated with all of the identical copies of a book in a library collection.  This class 
keeps track of a unified queue of pending holds, on the basis that the borrower doesn't care
which of the identical copies of the book they receive, as long as they receive a copy with the
particular lending rights they are comfortable with.

This data model, as implemented in the application, led to reasonably good separation of concerns, 
and resulted in the vast majority of data only being stored in a single place.  Every time you have
two copies of something, you create a synchronization problem, and I think I avoided that.  Looking at the UML diagram, in retrospect I wonder whether an inheritance hierarchy involving holds and borrowings
would have been appropriate given both represent associations between similar entities; however, 
I'm not sure it would have made things much simpler.

One subtle point along these lines was that I tried to avoid redundant status flags in the entity model; for instance, we don't have a enumeration defined to represent the status of a Borrowing, because
it was simpler to use whether the returnedDate attribute had been set to determine whether the 
book had been returned.

## The FieldQuery system

This is the sole use of an inheritance hierarchy in the application.  This was designed to allow
a flexible search mechanism where a client could make complex queries on the variety of possible fields on arbitrary item types (though only Books ended up being implemented).  

In essence, clients can submit a search request that contains a JSON blob with a list of 
field queries.  In the implementation, a query may be either a text query (representing a substring
search on that field) or a date query (allowing searching before a year, after a year, or within a 
year range).  All queries have a logical field associated with them.

The implementation deserializes this JSON blob into a list of the concrete subclasses, TextQuery and DateFieldQuery, of the superclass TextQuery.  FieldQuery has an abstract method, getPredicate, which
turns the deserialized FieldQueries into a Predicate, which is then composed using JPA query composition operations into a single composite query searching for the intersection of all the individual queries.

It involved harnessing quite a lot of framework magic, but the resulting query system is a nice demonstration of where inheritance hierarchies can be useful.

## Implementation

The implementation ended up containing many more classes than this, and in my view probably needs more to clean it up.

There are several groups of classes within the system and I have tried to mainatain a consistent 
naming scheme for them:

* _..Repository_ interfaces represent repositories of data model objects and their underlying database tables.  They support CRUD operations.  Spring and Hibernate magic automatically creates and instantiates these where required.
* _..Query_ classes are part of the FieldQuery system described above.  
* _..Exception_s are, you guessed it, exceptions.  The exception system is admittedly rudimentary and could do with a lot of improvement.
* _..Controller_s are the home of the actual API endpoints.  Like the _Repositories_, these get 
automatically instantiated by Spring.
* _..Info_ classes are classes created from instances of data models for the purpose of serialization
by Jackson.  Clever use of Jackson may have removed the need for some of these, but at the loss
of readability (at least for someone inexperienced with Jackson like myself).
* _ListHolder_ classes were basically a workaround for the Jackson JSON processor's allergy to generics; it seems that wrapping them in an object causes Jackson to behave.  Some of these may
be able to be refactored away.

## Issues with the design and implementation.

While I'm reasonably content with my logical data model, I'm not thrilled with the implementation.  
Due to my inexperience with the toolchain and architecture and the lack of useful documentation,  I made a bad mistake by pushing actual logic into the entity classes.  This was dumb, because:

* it meant that I was iterating over potentially large Java arraylists to find items that in a more
sane design I would have simply searched for.
* It meant that code for altering state for entity classes was separated from code from saving that state to the database.

My guess is the right solution here is to strip most or all of the logic out of the entity classes and place it in something like a Service class, which seems to be a common pattern in some types of Spring applications but I've not yet found an example of something similar using these automagic repositories and the restful endpoint Controllers.

I was also frankly surprised at how incredibly difficult it proved to reliably serialize and
deserialize JSON to POJO objects using Jackson, which led to the lack of a working web client.

While it perhaps getting offtopic for this document, I found Spring's documentation rather frustrating.  Introductory guides present tiny, trivial examples that don't give any useful indication about how to architect a bigger solution; Spring's reference documentation is comprehensive but impenetrable.  In the gap lies all manner of people on the internet who throw up example code but spectacularly fail to explain how and why it works.  