package org.rgmerk.librarycatalogue;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entity class representing information about when a borrower borrows a copy of a book.
 *
 * Borrowings that are active (ie the Borrower hasn't yet returned them) are represented
 * by the convention of having a null returnDate.  The convenience method isActive() should
 * be used to check this.
 * 
 * Completed borrowings have a non-null returnDate.
 * 
 * We should retain ALL Borrowings in the repository rather than deleting completed ones.
 */
@Entity 
public class Borrowing {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
    @JoinColumn(name= "fk_borrower")
    private Borrower borrower;
	
	@ManyToOne
    @JoinColumn(name = "fk_itemcopy")
	private
	ItemCopy itemcopy;
    
    @Column(name = "borrow_date", columnDefinition = "DATE")
    private LocalDate borrowDate;
    
    @Column(name = "due_date", columnDefinition = "DATE")
    private LocalDate dueDate;
    
    @Column(name = "returned_date", columnDefinition = "DATE")
    private LocalDate returnedDate;

    public Borrowing() {}
    
    
	/**
	 * Returns the date on which the loan was completed (book was returned).
	 * 
	 * If loan is still active, will return null.
	 * 
	 */
	public LocalDate getReturnedDate() {
		return returnedDate;
	}

	/**
	 * Sets the date on which the loan was completed (book was returned).
	 * 
	 * Set this to null if the loan is active.
	 */
	public void setReturnedDate(LocalDate returnedDate) {
		this.returnedDate = returnedDate;
	}

	public boolean isActive() {
		return returnedDate == null;
	}
	public ItemCopy getItemcopy() {
		return itemcopy;
	}

	public void setItemcopy(ItemCopy itemcopy) {
		this.itemcopy = itemcopy;
	}

	public LocalDate getBorrowDate() {
		return borrowDate;
	}

	public void setBorrowDate(LocalDate borrowDate) {
		this.borrowDate = borrowDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}


	public Borrower getBorrower() {
		return borrower;
	}


	public void setBorrower(Borrower borrower) {
		this.borrower = borrower;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
}
