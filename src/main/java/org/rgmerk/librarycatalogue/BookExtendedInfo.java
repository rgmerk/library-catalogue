package org.rgmerk.librarycatalogue;



/**
 * Class containing extended information about a book, intended specifically for JSON serialization.
 * 
 * Use the other Book classes for any other operations.
 * 
 * The information available includes:
 *  - the number of copies available in each library collection
 *  - whether a nominated borrower has any active borrowings, active or pending holds this book
 *    in any library collections.
 *  
 *  
 */
public class BookExtendedInfo {

	private long id;
	private String title;
	private String author;
	private String HTMLDescription;
	private int yearPublished;
	CollectionSetInfoListHolder collectionSets;
	
	public BookExtendedInfo(Book theBook, Borrower borrower) {
		this.setId(theBook.getId());
		this.setTitle(theBook.getTitle());
		this.setAuthor(theBook.getAuthor());
		this.setYearPublished(theBook.getYearPublished());
		this.HTMLDescription = buildHTMLDescription(getTitle(), getAuthor(), getYearPublished());
		collectionSets=new CollectionSetInfoListHolder();
		for (CollectionSet i: theBook.getCollectionsets()) {
			collectionSets.getCollectionSetInfos().add(new CollectionSetInfo(i, borrower));
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getYearPublished() {
		return yearPublished;
	}

	public void setYearPublished(int yearpublished) {
		this.yearPublished = yearpublished;
	}

	/**
	 * @return the collectionSets
	 */
	public CollectionSetInfoListHolder getCollectionSets() {
		return collectionSets;
	}

	private String buildHTMLDescription(String title, String author, int yearPublished) {
		StringBuilder str=new StringBuilder();
		str.append(String.format("<p><b>Title:</b> %s</p>%n", title));
		str.append(String.format("<p><b>Author:</b> %s</p>%n",author));
		str.append(String.format("<p><b>Year Published: %d</p>%n", yearPublished));
		return str.toString();
	}

	/**
	 * @return the hTMLDescription
	 */
	public String getHTMLDescription() {
		return HTMLDescription;
	}

	/**
	 * @param hTMLDescription the hTMLDescription to set
	 */
	public void setHTMLDescription(String hTMLDescription) {
		HTMLDescription = hTMLDescription;
	}
	
}
