package org.rgmerk.librarycatalogue;

/**
 * Exception to be thrown when a REST endpoint can't find a specified CollectionSet
 *
 * Handled by the EntityNotFoundAdvice machinery.
 */
public class CollectionSetNotFoundException extends EntityNotFoundException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3084676621371576578L;

	public CollectionSetNotFoundException(Long id) {
		super("collection set not found: " + id);
	}
}
