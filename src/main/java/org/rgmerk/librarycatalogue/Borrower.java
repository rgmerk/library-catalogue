package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity class representing an individual with borrowing permissions at this library. 
 *  
 */
@Entity
public class Borrower {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	//FIXME: this doesn't represent naming conventions adequately.
	private String familyName;
	private String givenName;

	//This is a list of ALL borrowings associated with this borrower, both
	// completed and active.
	@OneToMany(fetch=FetchType.LAZY, mappedBy="borrower", cascade=CascadeType.ALL)
	private List<Borrowing> borrowings=new ArrayList<Borrowing>();
	
	// this is a list of books CURRENTLY waiting on the hold shelf for this borrower.
	@OneToMany(fetch=FetchType.LAZY, mappedBy="holder", cascade=CascadeType.ALL)
	private List<ActiveHold> activeholds = new ArrayList<ActiveHold>();
	
	// this is a list of books which will be placed on the hold shelf for this borrower
	// when copies are returned to the library
	@OneToMany(fetch=FetchType.LAZY, mappedBy="holder", cascade=CascadeType.ALL)
	private List<PendingHold> pendingholds = new ArrayList<PendingHold>();
	
	
	protected Borrower() {}

	/**
	 * @param givenName Borrower's given name
	 * @param familyName Borrower's family name
	 */
	public Borrower(String givenName, String familyName) {
		this.givenName = givenName;
		this.familyName = familyName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setFirstName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	
	public String toString() {
		return "id: " + Long.toString(getId()) + givenName + " " + familyName;
	}

	public long getId() {
		return id;
	}

	/**
	 * @return ALL borrowings, active and completed.
	 */
	public List<Borrowing> getBorrowings() {
		return borrowings;
	}

	/**
	 * @return a list of currently active holds by this borrower.
	 */
	public List<ActiveHold> getActiveholds() {
		return activeholds;
	}

	/**
	 * @return a list of holds that are pending until a copy of the book is returned and can be put on 
	 * the hold shelf.
	 */
	public List<PendingHold> getPendingholds() {
		return pendingholds;
	}

	/**
	 * @param borrowings the borrowings to set
	 */
	public void setBorrowings(List<Borrowing> borrowings) {
		this.borrowings = borrowings;
	}

	/**
	 * @param activeholds the activeholds to set
	 */
	public void setActiveholds(List<ActiveHold> activeholds) {
		this.activeholds = activeholds;
	}

	/**
	 * @param pendingholds the pendingholds to set
	 */
	public void setPendingholds(List<PendingHold> pendingholds) {
		this.pendingholds = pendingholds;
	}
}
