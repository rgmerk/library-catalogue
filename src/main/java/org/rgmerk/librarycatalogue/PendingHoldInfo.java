package org.rgmerk.librarycatalogue;

/**
 * Holds information about a PendingHold in a format suitable for 
 * JSON serialization and export via the RESTFul interface.
 * 
 * For convenience, includes the bookId so you don't have to make a further call
 * to look it up.
 * 
 * Created directly from an individual PendingHold by the constructor.
 * 
 * Part of the information presented in a BookExtendedInfo.
 * 
 * Do not use for any other purpose.
 * 
 */

public class PendingHoldInfo {
	private long id;
	private long collectionSetId;
	private long bookId;
	private String requestDateTime;
	
	public PendingHoldInfo() {}
	
	public PendingHoldInfo(PendingHold p) {
		this.id = p.getId();
		this.collectionSetId=p.getCollectionSet().getId();
		this.bookId = p.getCollectionSet().getBook().getId();
		this.requestDateTime = p.getRequestDateTime().toString();
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the collectionSetid
	 */
	public long getCollectionSetId() {
		return collectionSetId;
	}
	/**
	 * @param collectionSetid the collectionSetid to set
	 */
	public void setCollectionSetId(long collectionSetid) {
		this.collectionSetId = collectionSetid;
	}
	/**
	 * @return the bookId
	 */
	public long getBookId() {
		return bookId;
	}
	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(long bookId) {
		this.bookId = bookId;
	}
	/**
	 * @return the requestDateTime
	 */
	public String getRequestDateTime() {
		return requestDateTime;
	}
	/**
	 * @param requestDateTime the requestDateTime to set
	 */
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}
	
	
}
