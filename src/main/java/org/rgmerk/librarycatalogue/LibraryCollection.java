package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * A collection of CollectionSets (sets of copies of specific Books) that represents a "collection"
 * in a large library.
 * 
 * Items in a Collection are often physically located contiguously within a library, 
 * have a distinct name (typically something like "main collection" or "three day loan collection"),
 *  and borrowing privileges extend on
 * a per-collection basis.  In this case, this is represented by a single attribute, a borrowing 
 * duration that indicates the number of days items in this collection are permitted to be borrowed.
 *
 *
 */
@Entity
public class LibraryCollection {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	private int borrowingLength;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="collection")
	List<CollectionSet> collectionSets = new ArrayList<CollectionSet>();
	
	protected LibraryCollection() {}
	
	/**
	 * @param name the name of this collection
	 * @param borrowingLength the duration of permitted borrowings of items in this collection
	 */
	public LibraryCollection(String name, int borrowingLength) {
		this.name = name;
		this.borrowingLength = borrowingLength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBorrowingLength() {
		return borrowingLength;
	}

	public void setBorrowingLength(int borrowingLength) {
		this.borrowingLength = borrowingLength;
	}

	public List<CollectionSet> getCollectionSets() {
		return collectionSets;
	}
	
	/**
	 * add a CollectionSet.  
	 * 
	 * Note that this will remove the collectionSet from its previous LibraryCollection.
	 *  
	 */
	public void addCollectionSet(CollectionSet set) {
		collectionSets.add(set);
		set.setCollection(this);
	}
	
}
