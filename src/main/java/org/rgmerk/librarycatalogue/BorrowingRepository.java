package org.rgmerk.librarycatalogue;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRUD repository for Borrowings.  Automatically instantiated in the Controllers.
 * 
 * Due to the magic of Spring Data the implementation is automatically generated.
 *
 */
@Transactional
public interface BorrowingRepository extends CrudRepository<Borrowing, Long> {

	Borrowing findById(long id);
	List<Borrowing> findAll();

}