package org.rgmerk.librarycatalogue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


/**
 * An entity class representing a copy of a Book.
 * 
 * Books are part of CollectionSets (representing multiple copies of a Book) held in a LibraryCollection.
 * 
 * Borrowings and ActiveHolds (where the book is currently on the "hold shelf" are associated
 * with an individual ItemCopy.  Pending holds are associated with the parent CollectionSet.
 * 
 * FIXME: in retrospect, pretty much all the logic should be taken out of the entity class and
 * placed somewhere else, probably in some kind of Service class which would have access to 
 * the repository and would be able to use database operations for performance reasons, and 
 * manage updates etc.
 *
 */
@Entity
public class ItemCopy {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne 
	@JoinColumn(name="fk_collectionset")
	private CollectionSet collectionSet;
	
	// NOTE: we don't track past active holds.  Maybe we should.
	@OneToOne
	private ActiveHold activeHold;
	
	// We track all borrowings, even completed ones.
	@OneToMany(fetch=FetchType.LAZY, mappedBy="itemcopy", cascade=CascadeType.ALL)
	private List<Borrowing> borrowings=new ArrayList<Borrowing>();

	public ItemCopy() {}
	
	/**
	 * returns the CollectionSet associated with this ItemCopy
	 */
	public CollectionSet getCollectionSet() {
		return collectionSet;
	}

	/**
	 * set the CollectionSet associated with this ItemCopy.
	 * 
	 */
	public void setCollectionSet(CollectionSet collectionSet) {
		this.collectionSet = collectionSet;
	}
	
	/**
	 * return true if this item currently has an active Borrowing.
	 * 
	 * FIXME: refactor out of entity class so we can use DB search operations
	 */
	public boolean isBorrowed() {
		for (Borrowing b: borrowings) {
		
			if(b.getReturnedDate() == null) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * return true if this item has an active hold.
	 */
	public boolean isActivelyHeld() {
		return (getActiveHold() != null);
	}
	
	/**
	 * set an active hold on this item. 
	 * 
	 * Remember to save the ActiveHold.
	 * 
	 * FIXME: refactor out of entity class so ActiveHold can be saved as part of the operation.
	 * @param hold
	 */
	public void setActiveHold(ActiveHold hold) {
		activeHold = hold;
		if (hold != null) {
			hold.setItemCopy(this);
		}
	}
	
	/**
	 * clear the active hold on this item.
	 * 
	 * Remember to delete the ActiveHold
	 * 
	 * FIXME: refactor out of entity class so ActiveHold can be deleted as part of the operation.
	 */
	public void clearActiveHold() {
		getActiveHold().setItemCopy(null);
		activeHold = null;
	}

	/**
	 * Borrow this itemCopy.
	 * 
	 * If your Borrower has an active hold on this item, they will need to clear it first.
	 * 
	 * @throws NotPermittedException if the item is not available for borrowing.
	 */
	public void addBorrowing(Borrowing borrowing) throws NotPermittedException {
		if(!isAvailable()) {
			throw new NotPermittedException("Book is not available for borrowing");
		}
		borrowings.add(borrowing);
		borrowing.setItemcopy(this);
		
	}
	
	/**
	 * Return this itemCopy.
	 * 
	 * Doesn't update holds.
	 * 
	 * @throws NotPermittedException if the book has not been borrowed.
	 */
	public void returnCopy() throws NotPermittedException {
		if(!isBorrowed()) {
			throw new NotPermittedException("Book has not been borrowed");
		}
		Borrowing activeBorrowing = getActiveBorrowing();
		
		activeBorrowing.setReturnedDate(LocalDate.now());
		
		
		
	}

	/**
	 * Return the active (non-completed) borrowing for this ItemCopy, or null if there are none.
	 * 
	 * By definition there should only be one such borrowing!
	 * 
	 * FIXME: refactor out of entity class so we can use database operations to speed search.
	 */
	public Borrowing getActiveBorrowing() {
		for(Borrowing b: borrowings) {
			if (b.getReturnedDate() == null) {
				return b;
			}
		}
		return null;
	}
	public String toString() {
		return "id: " + Long.toString(getId());
	}

	public long getId() {
		return id;
	}

	/**
	 * returns the current ActiveHold.
	 * 
	 * Note that the system does not currently keep records of past active holds.
	 * 
	 */
	public ActiveHold getActiveHold() {
		return activeHold;
	}

	/**
	 * place an active hold on this ItemCopy, if currently available.
	 * @param borrower the borrower to hold this ItemCopy for
	
	 * @throws NotPermittedException if copy is not available
	 */
	public ActiveHold placeActiveHold(Borrower borrower) throws NotPermittedException {
		if (!isAvailable()) {
			throw new NotPermittedException("Book copy is not available for active hold");
		}
		activeHold = new ActiveHold();
		activeHold.setItemCopy(this);
		activeHold.setHolder(borrower);
		return activeHold;
	}
	
	/**
	 * return true if the book is currently available for borrowing or an active hold
	 */
	public boolean isAvailable() {
		
		return !(isBorrowed()) && !(isActivelyHeld());
	}
	
}
