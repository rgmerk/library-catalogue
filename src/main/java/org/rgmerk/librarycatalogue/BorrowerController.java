package org.rgmerk.librarycatalogue;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Creates RESTful endpoints relating to an individual borrower.
 *
 * This class should never need to be instantiated manually. Spring Boot will do
 * it for you.
 * 
 */
@RestController
class BorrowerController {

	private static final Logger log = LoggerFactory.getLogger(BorrowerController.class);

	private final BorrowerRepository repository;
	private final ItemCopyRepository itemCopyRepository;
	private final BorrowingRepository borrowingRepository;
	private final CollectionSetRepository collectionSetRepository;
	private final ActiveHoldRepository activeHoldRepository;
	private final PendingHoldRepository pendingHoldRepository;

	/**
	 * Don't invoke this manually.
	 */
	public BorrowerController(BorrowerRepository repository, ItemCopyRepository itemCopyRepository,
			BorrowingRepository borrowingRepository, CollectionSetRepository collectionSetRepository,
			ActiveHoldRepository activeHoldRepository, PendingHoldRepository pendingHoldRepository) {
		this.repository = repository;
		this.itemCopyRepository = itemCopyRepository;
		this.borrowingRepository = borrowingRepository;
		this.collectionSetRepository = collectionSetRepository;
		this.activeHoldRepository = activeHoldRepository;
		this.pendingHoldRepository = pendingHoldRepository;
	}

	/**
	 * Endpoint returning basic information about all borrowers in the system.
	 */
	@GetMapping("/borrowers")
	List<Borrower> all() {
		return repository.findAll();
	}

	// Single item

	/**
	 * Endpoint returning basic information about a specific borrower identified by their id
	 * @param id the borrower id
	 */
	@GetMapping("/borrower/{id}")
	Borrower one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new BorrowerNotFoundException(id));
	}

	/**
	 * Endpoint returning detailed information about a specific borrower identified by their id
	 * 
	 * This returns information about the borrower's current borrowings, active, and pending holds
	 * @param id the borrower id
	 * @return
	 */
	@GetMapping("borrower/{id}/extendedinfo")
	BorrowerExtendedInfo getExtendedInfo(@PathVariable Long id) {
		Borrower borrower = repository.findById(id).orElseThrow(() -> new BorrowerNotFoundException(id));
		return new BorrowerExtendedInfo(borrower);
	}

	/**
	 * Endpoint to borrow a copy of a book.  Returns a status string
	 * 
	 * @param id - the borrower ID
	 * @param copyid the ItemCopy id
	 * @return a status string based on the success of the borrowing.
	 */
	@PostMapping("/borrower/{id}/borrow/{copyid}")
	String borrowItem(@PathVariable Long id, @PathVariable Long copyid) {
		
		Borrower borrower = repository.findById(id)
				.orElseThrow(() -> new BorrowerNotFoundException(id));
		ItemCopy copy = itemCopyRepository.findById(copyid)
				.orElseThrow(() -> new BorrowerNotFoundException(id));

		if (copy.getActiveBorrowing() != null) {
			return "Book is already borrowed.";
		}
		
		// if borrower has the active hold in this copy clear it
		// if anyone else has the active hold on this copy don't let it be borrowed.
		ActiveHold hold = copy.getActiveHold();
		if (hold != null) {
			if (hold.getHolder().getId() == borrower.getId()) {
				cancelActiveHold(copy, hold);
			} else {
				return "Book is actively held by someone else.  You can't borrow this now!";
			}
		}
		
		// create the borrowing record and add it to the ItemCopy
		Borrowing borrowing = createBorrowing(borrower, copy);
		copy.addBorrowing(borrowing);
		itemCopyRepository.save(copy);
	
		return "Successfully borrowed";
	}




	/**
	 * Endpoint for returning the ItemCopy identified by the copyid.
	 * 
	 * If there are pending holds for the parent CollectionSet, choose the earliest
	 * one and put an active hold on the ItemCopy for the intended holder.
	 * 
	 * @return a status string
	 */
	@PostMapping("/return/{copyid}")
	String returnItem(@PathVariable Long copyid) {
		// FIXME: more appropriate exception type
		ItemCopy copy = itemCopyRepository.findById(copyid).orElseThrow(() -> new BorrowerNotFoundException(copyid));
		copy.returnCopy();
		// if there's a pending hold for actively held items in the relevant
		// collectionSet,
		// make an active hold

		if (copy.getCollectionSet().getPendingHoldCount() > 0) {
			transferHoldfromPending(copy);

		}
		itemCopyRepository.save(copy);
		return "Successfully returned";
	}

	/**
	 * Endpoint for setting a hold for this collection set.
	 * 
	 * If an itemcopy is available, set an active hold on one (the choice of item is arbitrary).  
	 * If no itemcopy is available, set a pending hold on the collectionSet. 
	 * 
	 * Note that a borrower can only have one hold, be it active or passive, on an item in
	 * a specific collectionSet.
	 * 
	 * @param id the borrower id
	 * @param collectionsetid the collection on which the borrower wants to set a hold
	 * @return a status string
	 */
	@PostMapping("/borrower/{id}/hold/{collectionsetid}")
	// FIXME: exception type
	String holdItem(@PathVariable Long id, @PathVariable Long collectionsetid) {
		CollectionSet collectionSet = collectionSetRepository.findById(collectionsetid)
				.orElseThrow(() -> new BorrowerNotFoundException(collectionsetid));
		Borrower borrower = repository.findById(id).orElseThrow(() -> new BorrowerNotFoundException(id));
		if (collectionSet.haveActiveHold(borrower)) {
			return "You already have an active hold on this collection item.  Check the hold shelf";
		}

		if (collectionSet.havePendingHold(borrower)) {
			return "No copies are available yet but you are in the queue for a hold.";
		}

		ItemCopy availableCopy = collectionSet.getAvailableCopy();
		if (availableCopy != null) {
			ActiveHold activeHold = availableCopy.placeActiveHold(borrower);
			activeHoldRepository.save(activeHold);
			itemCopyRepository.save(availableCopy);
			return "Active hold placed.  Check the hold shelf!";
		} else {
			PendingHold pendingHold = new PendingHold(collectionSet, borrower);
			pendingHoldRepository.save(pendingHold);
			return "No copies available.  You've been added to the hold queue for this item";
		}

	}
	
	/**
	 * Cancel any hold the borrower may have on an item in the CollectionSet
	 * 
	 * Note: there can only be a total of one hold placed by a borrower on a CollectionSet 
	 * @param id
	 * @param collectionsetid
	 * @return
	 */
	@PostMapping("/borrower/{id}/cancelhold/{collectionsetid}")
	String cancelHoldItem(@PathVariable Long id, @PathVariable Long collectionsetid) {
		CollectionSet collectionSet = collectionSetRepository.findById(collectionsetid)
				.orElseThrow(() -> new BorrowerNotFoundException(collectionsetid));
		Borrower borrower = repository.findById(id).orElseThrow(() -> new BorrowerNotFoundException(id));
		ActiveHold activeHold = collectionSet.getActiveHold(borrower);
	
		if (activeHold != null) {
			ItemCopy copy = activeHold.getItemCopy();
			cancelActiveHold(copy, activeHold);
			return "Active hold cancelled";
		}
		
		PendingHold pendingHold = collectionSet.getPendingHold(borrower);
		if (pendingHold != null) {
			collectionSet.clearPendingHold(pendingHold);
			collectionSetRepository.save(collectionSet);
			return "Pending hold cancelled";
		}

		return "Borrower does not have an active or pending hold on this collection set";
	}
	/**
	 * Create and save a Borrowing record.
	 * @param borrower
	 * @param copy
	 * @return the created borrowing.
	 */
	private Borrowing createBorrowing(Borrower borrower, ItemCopy copy) {
		Borrowing borrowing = new Borrowing();
		borrowing.setBorrower(borrower);
		int borrowingduration = copy.getCollectionSet().getCollection().getBorrowingLength();
		borrowing.setBorrowDate(LocalDate.now());
		borrowing.setDueDate(LocalDate.now().plusDays(borrowingduration));
		return borrowing;
	}
	
	/**
	 * Take the earliest pending hold on the ItemCopy's parent collectionSet 
	 * and make an active hold based on it.
	 *  
	 * @param copy
	 * @param collectionSet
	 */
	private void transferHoldfromPending(ItemCopy copy) {
		CollectionSet collectionSet = copy.getCollectionSet();
		PendingHold p = collectionSet.getFirstPendingHold();
		ActiveHold activeHold = copy.placeActiveHold(p.getHolder());
		collectionSet.clearPendingHold(p);
		activeHoldRepository.save(activeHold);
		collectionSetRepository.save(collectionSet);
		itemCopyRepository.save(copy);
		activeHoldRepository.save(activeHold);
	}


	/**
	 * Cancel an active hold on an itemcopy.
	 * 
	 * FIXME: should be moved to a service class when one exists.
	 * @param copy
	 * @param hold
	 * 
	 */
	private void cancelActiveHold(ItemCopy copy, ActiveHold hold) {
		copy.setActiveHold(null);
		itemCopyRepository.save(copy);
		activeHoldRepository.delete(hold);
	}


}
