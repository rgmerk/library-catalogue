package org.rgmerk.librarycatalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A wrapper around a list of BorrowingInfo objects, used for JSON serialization.
 * 
 * FIXME: I am not sure this is absolutely required but Jackson's ObjectMapper seems to get
 * horribly confused by generics and this seems to be the magic workaround.  
 */
public class BorrowingsInfoListHolder {
	private List <BorrowingInfo> borrowings;
	
	@JsonCreator
	BorrowingsInfoListHolder(@JsonProperty("borrowings") List<BorrowingInfo> borrowings) {
		this.borrowings=borrowings;
	}

	/**
	 * @return the borrowings
	 */
	public List <BorrowingInfo> getBorrowings() {
		return borrowings;
	}

	/**
	 * @param borrowings the borrowings to set
	 */
	public void setBorrowings(List <BorrowingInfo> borrowings) {
		this.borrowings = borrowings;
	}
}
