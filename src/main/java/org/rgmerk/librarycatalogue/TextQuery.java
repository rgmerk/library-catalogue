package org.rgmerk.librarycatalogue;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * One of the query types which BookQuerySpecification magically combines into an actual query 
 * Specification.
 * 
 * This class is intended to be deserialized from a JSON-formatted request body and generate
 * a query on one of the text fields in Book 
 * 
 * The following is an example of a TextQuery object JSON-serialized:
 * 
 * {"className":"org.rgmerk.librarycatalogue.TextQuery","field":"title","querytext":"Animal"}
 *
 * This generates a predicate which matches all books where the title field contains the text 
 * Animal.
 * 
 */
public class TextQuery extends FieldQuery {
	private String field;
	private String querytext;
	
	@JsonCreator
	public TextQuery(@JsonProperty("field") String field, 
			@JsonProperty("querytext") String querytext) {
		this.field=field;
		this.querytext = querytext;
	}

	public String getField() {
		return field;
	}

	public String getQuerytext() {
		return querytext;
	}

	@Override
	public Predicate getPredicate(Root<Book> root, CriteriaBuilder cb) throws InvalidQueryException {
		try {
			// TODO: filter metacharacters from querytext or find a better solution
			// TODO: filter fields to stop the privacy leak
			// Make a predicate that matches where the field contains the querytext.
			Predicate p = cb.like(root.get(field), "%"+ querytext + "%");
			return p;
		} catch (Exception e) {
			throw new InvalidQueryException();
		}
	
	}
}
