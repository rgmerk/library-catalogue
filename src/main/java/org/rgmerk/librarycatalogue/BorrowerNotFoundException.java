package org.rgmerk.librarycatalogue;

/**
 * Exception to be thrown if a controller can't find a borrower
 * 
 */
class BorrowerNotFoundException extends EntityNotFoundException {

	BorrowerNotFoundException(Long id) {
		super("Could not find borrower " + id);
	}
}
