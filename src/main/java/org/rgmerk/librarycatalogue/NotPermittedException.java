package org.rgmerk.librarycatalogue;

/**
 * An exception thrown in the model when an attempt is made to do something which you're
 * not permitted to do, such as borrow an already borrowed book.
 *
 * I'm sure I could do a better Exception class hierarchy given time and more understanding
 * of Exception class hierarchies but this will do for now.
 */
public class NotPermittedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8412273643272170812L;

	NotPermittedException(String reason) {
		super(reason);
	}
}
