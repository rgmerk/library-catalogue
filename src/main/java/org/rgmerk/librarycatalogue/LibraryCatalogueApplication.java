package org.rgmerk.librarycatalogue;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Main class of the Library catalogue application.
 * 
 * The code in the demo commandlinerunner is executed at startup.
 * 
 * 
 * @author Robert Merkel <robert.merkel@benambra.org>
 *
 */
@SpringBootApplication
public class LibraryCatalogueApplication {


	//de-facto stderr channel.
	private static final Logger log = LoggerFactory.getLogger(LibraryCatalogueApplication.class);

	/*
	 * This is one of the first of many pieces of Spring magic in this application.
	 * 
	 * Do not modify this; modify the demo method below.
	 */
	public static void main(String[] args) {
		SpringApplication.run(LibraryCatalogueApplication.class, args);
	}

	/**
	 * The de-facto main method of this application.  Run at startup, and
	 * lets us populate the database with a few elements so we can play with
	 * them through the RESTful endpoints.
	 * 
	 * 
	 */
	@Bean
	public CommandLineRunner demo(BorrowerRepository borrowers, 
			BookRepository books, 
			ItemCopyRepository itemCopies,
			LibraryCollectionRepository collections,
			CollectionSetRepository collectionSets) {
		return (args) -> {
			// save a few borrowers
			borrowers.save(new Borrower("Jack", "Bauer"));
			borrowers.save(new Borrower("Chloe", "O'Brian"));
			borrowers.save(new Borrower("Kim", "Bauer"));
			borrowers.save(new Borrower("David", "Palmer"));
			borrowers.save(new Borrower("Michelle", "Dessler"));
			
			// a few books.
			Book orwell = books.save(new Book("Nineteen Eighty-Four", "George Orwell", 1948));
			Book huxley = books.save(new Book("Brave New World", "Aldous Huxley", 1931));
			Book animal = books.save(new Book("Animal Farm", "George Orwell", 1952));
			
			LibraryCollection mainCollection = collections.save(
					new LibraryCollection("Main Collection", 21));
			
		
			// collectionsets for the books.
			CollectionSet orwellSet = collectionSets.save(new CollectionSet(orwell));
			CollectionSet animalSet = collectionSets.save(new CollectionSet(animal));
			CollectionSet huxleySet = collectionSets.save(new CollectionSet(huxley));

			//  copies of each of the books.
			ItemCopy orwellCopy = itemCopies.save(new ItemCopy());
			ItemCopy orwellCopy2 = itemCopies.save(new ItemCopy());
			ItemCopy animalCopy = itemCopies.save(new ItemCopy());
			ItemCopy huxleyCopy = itemCopies.save(new ItemCopy());
	
			// add the collectionSets to the collection
			
			mainCollection.addCollectionSet(orwellSet);
			mainCollection.addCollectionSet(animalSet);
			mainCollection.addCollectionSet(huxleySet);
			mainCollection = collections.save(mainCollection);

			// add the copies to the CollectionSets
			
			orwellSet.addCopy(orwellCopy);
			orwellSet.addCopy(orwellCopy2);
			animalSet.addCopy(animalCopy);
			huxleySet.addCopy(huxleyCopy);
			
		//	log.info("OrwellCopy collection set is " + orwellCopy.getCollectionSet().toString());
			collectionSets.save(orwellSet);
			collectionSets.save(animalSet);
			collectionSets.save(huxleySet);
			
		// ensure the itemcopies are saved.
			orwellCopy = itemCopies.save(orwellCopy);
			orwellCopy2 = itemCopies.save(orwellCopy2);
			animalCopy = itemCopies.save(animalCopy);
			huxleyCopy = itemCopies.save(huxleyCopy);
			//log.info("OrwellCopy collection set is " + orwellCopy.getCollectionSet().toString());

		// do some sanity checking on the stored data.
			List <Borrower> allBorrowers = borrowers.findAll();
			for (Borrower x: allBorrowers) {
				log.info(x.toString());
			}
			
			log.info("");
			List <Book> allBooks = books.findAll();
			for (Book b: allBooks) {
				log.info(b.toString());
			}
			
			log.info("");
			
			log.info("selective search");
			
			FieldQueryListHolder searchqueryholder = new FieldQueryListHolder();
			List <FieldQuery> queries = new ArrayList<FieldQuery>();
			queries.add(new DateFieldQuery("yearPublished", new Integer(1947), new Integer(1952)));
			queries.add(new TextQuery("title", "Animal"));
			
			searchqueryholder.setQueries(queries);
			
			ObjectMapper o = new ObjectMapper();
			String json = o.writeValueAsString(searchqueryholder);
			FieldQueryListHolder deserializedHolder = o.readValue(json, FieldQueryListHolder.class);
			log.info("searchquery JSON");
			log.info(json);
			log.info("");
			List <Book> selectedBooks = books.findAll(BookQuerySpecification.matchesQueryFields(deserializedHolder.getQueries()));
			
			for (Book b: selectedBooks) {
				log.info(b.toString());
			}
			
			List <ItemCopy> allcopies = itemCopies.findAll();
			for (ItemCopy i : allcopies) {
				log.info(i.toString());
			}
			
			log.info("");
		};
	}
}
