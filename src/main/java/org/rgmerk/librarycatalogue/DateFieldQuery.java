package org.rgmerk.librarycatalogue;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * One of the query types which BookQuerySpecification magically combines into an actual query 
 * specification.
 * 
 * This class is intended to be deserialized from a JSON-formatted request body and generate
 * a query on one of the date fields in Book (currently there is only one, but it is designed
 * to be extensible).  
 * 
 * The following is an example of a DateFieldQuery object JSON-serialized:
 * 
 * {"className":"org.rgmerk.librarycatalogue.DateFieldQuery","field":"yearPublished","from":1947,"to":1952}
 *
 * This generates a predicate which matches all books where the yearPublished is >= 1947 and <= 1952
 * If from was instead null, the predict would match all books where yearPublished <=1952
 * If to was instead null but from was 1947, this would match all books where the yearPublished >=1947
 * 
 * If both fields are null, it generates a predicate that matches all books (a dummy predicate).
 */
public class DateFieldQuery extends FieldQuery {
	
	private String field;
	// use objects to allow them to be null.
	private Integer from;
	private Integer to;
	
	/**
	 * Intended mainly for the JSON deserializer.  See class comments for more detailed explanation.
	 * @param field - the field this predicate filters on
	 * @param from - the year to match from
	 * @param to - the year to match to
	 */
	@JsonCreator
	public DateFieldQuery(@JsonProperty("field") String field,
			@JsonProperty("from")Integer from, 
			@JsonProperty("to") Integer to)   {
	//Don't do validation here to allow the deserialization to do its job
	//	- do it when we actually turn it into a real query.
		this.field = field;
		this.from = from;
		this.to = to;
	}
	
	public Integer getFrom() {
		return from;
	}
	
	public Integer getTo() {
		return to;
	}
	public String toString() {
		return "from: " + from.toString() + "to : " + to.toString();
	}

	public String getField() {
		return field;
	}

	/**
	 * generate the predicate.  See parent class for fuller explanation.
	 */
	@Override
	public Predicate getPredicate(Root<Book> root, CriteriaBuilder cb) throws InvalidQueryException {
		try {
			// TODO: filter fields to stop the potential privacy issue 
			Predicate topred=null, frompred=null;
			
			if(from != null) {
				frompred = cb.ge(root.get(field), from.intValue());
			}
			if(to != null) {
				topred = cb.le(root.get(field), to.intValue());
			}
			
			if(frompred != null && topred !=null) {
				return cb.and(frompred, topred);
			}
			if(frompred != null) {
				return frompred;
			}
			
			if(topred != null) {
				return topred;
			}
			
			/*
			 * if they're empty, return something that's always true
			 * FIXME: eliminate this always-true predicate for performance reasons.
			 */
			return cb.isTrue(cb.literal(true));
		} catch (Exception e) {
			throw new InvalidQueryException();
		}
	}
}
