package org.rgmerk.librarycatalogue;

/**
 * Holds information about a CollectionSet in a format suitable for 
 * JSON serialization and export via the RESTFul interface.
 * 
 * Includes information about whether a specific borrower has borrowed,
 * or holds active or passive holds on items in this CollectionSet.
 * 
 * Created directly from an individual CollectionSet by the constructor.
 * 
 * Part of the information presented in a BookExtendedInfo.
 * 
 * Do not use for any other purpose.
 * 
 */
public class CollectionSetInfo {

	private long id;
	private String collectionName;
	private int copiesAvailable;
	private int copiesOnLoan;
	private int copiesOnHoldShelf;
	private int holdQueueLength;
	private boolean haveIt;
	private boolean haveActiveHold;
	private boolean inHoldQueue;

	/**
	 * @param set - the CollectionSet we're constructing this from
	 * @param borrower - the borrower we're checking their current interactions with set
	 */
	public CollectionSetInfo(CollectionSet set, Borrower borrower) {
		this.setId(set.getId());
		this.setCollectionName(set.getCollection().getName());
		this.setCopiesAvailable(0);
		this.setCopiesOnLoan(0);
		this.setCopiesOnHoldShelf(0);
		this.setHaveIt(false);
		
		for(ItemCopy i : set.getCopies()) {
			if (i.isBorrowed()) {
				setCopiesOnLoan(getCopiesOnLoan() + 1);
				if(i.getActiveBorrowing().getBorrower().getId() == borrower.getId()) {
					setHaveIt(true);
				}
			} else if (i.isActivelyHeld()) {
				setCopiesOnHoldShelf(getCopiesOnHoldShelf() + 1);
				if(i.getActiveHold().getHolder().getId() == borrower.getId()) {
					setHaveActiveHold(true);
				}
			} else {
				setCopiesAvailable(getCopiesAvailable() + 1);
			}
		}
		
		this.setInHoldQueue(set.havePendingHold(borrower));
		this.setHoldQueueLength(set.getPendingHoldCount());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the collectionName
	 */
	public String getCollectionName() {
		return collectionName;
	}

	/**
	 * @param collectionName the collectionName to set
	 */
	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	/**
	 * @return the copiesAvailable
	 */
	public int getCopiesAvailable() {
		return copiesAvailable;
	}

	/**
	 * @param copiesAvailable the copiesAvailable to set
	 */
	public void setCopiesAvailable(int copiesAvailable) {
		this.copiesAvailable = copiesAvailable;
	}

	/**
	 * @return the copiesOnLoan
	 */
	public int getCopiesOnLoan() {
		return copiesOnLoan;
	}

	/**
	 * @param copiesOnLoan the copiesOnLoan to set
	 */
	public void setCopiesOnLoan(int copiesOnLoan) {
		this.copiesOnLoan = copiesOnLoan;
	}

	/**
	 * @return the copiesOnHoldShelf
	 */
	public int getCopiesOnHoldShelf() {
		return copiesOnHoldShelf;
	}

	/**
	 * @param copiesOnHoldShelf the copiesOnHoldShelf to set
	 */
	public void setCopiesOnHoldShelf(int copiesOnHoldShelf) {
		this.copiesOnHoldShelf = copiesOnHoldShelf;
	}

	/**
	 * @return the holdQueueLength
	 */
	public int getHoldQueueLength() {
		return holdQueueLength;
	}

	/**
	 * @param holdQueueLength the holdQueueLength to set
	 */
	public void setHoldQueueLength(int holdQueueLength) {
		this.holdQueueLength = holdQueueLength;
	}

	/**
	 * @return the haveIt
	 */
	public boolean isHaveIt() {
		return haveIt;
	}

	/**
	 * @param haveIt the haveIt to set
	 */
	public void setHaveIt(boolean haveIt) {
		this.haveIt = haveIt;
	}

	/**
	 * @return the haveActiveHold
	 */
	public boolean isHaveActiveHold() {
		return haveActiveHold;
	}

	/**
	 * @param haveActiveHold the haveActiveHold to set
	 */
	public void setHaveActiveHold(boolean haveActiveHold) {
		this.haveActiveHold = haveActiveHold;
	}

	/**
	 * @return the inHoldQueue
	 */
	public boolean isInHoldQueue() {
		return inHoldQueue;
	}

	/**
	 * @param inHoldQueue the inHoldQueue to set
	 */
	public void setInHoldQueue(boolean inHoldQueue) {
		this.inHoldQueue = inHoldQueue;
	}
	

}
