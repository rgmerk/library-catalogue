package org.rgmerk.librarycatalogue;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for ItemCopies.  See ItemCopy for more details.
 *
 * Implementation of CRUDRepositories are automatically created and instantiated 
 * by Spring Boot where required.
 */
@Transactional
public interface ItemCopyRepository extends CrudRepository<ItemCopy, Long> {

	ItemCopy findById(long id);
	List<ItemCopy> findAll();

}
