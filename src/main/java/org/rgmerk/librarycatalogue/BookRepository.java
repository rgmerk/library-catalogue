package org.rgmerk.librarycatalogue;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * An interface for doing CRUD actions on Books.
 *
 * The actual implementation is generated automagically by Spring Data.
 * 
 * This particular repository also extends the JPASpecificationExecutor interface, which 
 * permits a findAll(Specification<Book> specification) operation where the Repository returns
 * all books matching the specification.  This is used in the BookController.search method.  
 * See the BookQuerySpecification class for an example of a method that creates a Specification<Book>.
 */
@Transactional
public interface BookRepository extends CrudRepository<Book, Long>, JpaSpecificationExecutor<Book>  {

	Book findById(long id);
	List<Book> findAll();

}