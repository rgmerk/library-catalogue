package org.rgmerk.librarycatalogue;

public class EntityNotFoundException extends RuntimeException {

	/**
	 * A superclass for all of the Entity not found exceptions, so that a single response generator
	 * (EntityNotFoundAdvice) can handle them all.
	 */
	private static final long serialVersionUID = 1L;

	public EntityNotFoundException(String message) {
		super(message);
	}

	
}