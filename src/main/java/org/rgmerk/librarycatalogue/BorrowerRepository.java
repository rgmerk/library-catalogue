package org.rgmerk.librarycatalogue;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRUD repository for Borrowers.  
 *
 */
@Transactional
public interface BorrowerRepository extends CrudRepository<Borrower, Long> {

//	List<Borrower> findByFamilyName(@Param("name") String name);
	Borrower findById(long id);
	List<Borrower> findAll();

}