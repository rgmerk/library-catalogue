package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

/**
 * A class created to hold detailed information about a Borrower, including 
 * details of their active borrowings, and active and pending holds,
 * in a format suitable for serialization by Jackson.
 * 
 * Used in BorrowerController.GetExtendedInfo endpoint.
 * 
 * Use the other Borrower classes for all other purposes.
 * 
 */

public class BorrowerExtendedInfo {
	private long id;
	private String familyName;
	private String givenName;
	private BorrowingsInfoListHolder activeBorrowings;
	private ActiveHoldsInfoListHolder activeHolds;
	private PendingHoldsInfoListHolder pendingHolds;
	
	public BorrowerExtendedInfo(Borrower borrower) {
		this.id = borrower.getId();
		this.familyName = borrower.getFamilyName();
		this.givenName = (borrower.getGivenName());
		fillActiveBorrowings(borrower);
		fillActiveHolds(borrower);
		fillPendingHolds(borrower);
		
	}
	
	private void fillPendingHolds(Borrower borrower) {
		List<PendingHoldInfo> pendings = new ArrayList<PendingHoldInfo>();
		for (PendingHold p : borrower.getPendingholds()) {
			pendings.add(new PendingHoldInfo(p));
		}
		this.pendingHolds = new PendingHoldsInfoListHolder(pendings);
	}

	private void fillActiveHolds(Borrower borrower) {
		List <ActiveHoldInfo> actives = new ArrayList<ActiveHoldInfo>();
		
		for (ActiveHold h : borrower.getActiveholds()) {
			actives.add(new ActiveHoldInfo(h));
		}
		this.activeHolds = new ActiveHoldsInfoListHolder(actives);
	
	}
	private void fillActiveBorrowings(Borrower borrower) {
		List <BorrowingInfo> actives = new ArrayList<BorrowingInfo>();
		for(Borrowing b : borrower.getBorrowings()) {
			if (b.isActive()) {
				actives.add(new BorrowingInfo(b));
			}
		}
		activeBorrowings = new BorrowingsInfoListHolder(actives);
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * @param familyName the familyName to set
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the activeBorrowings
	 */
	public BorrowingsInfoListHolder getActiveBorrowings() {
		return activeBorrowings;
	}

	/**
	 * @return the activeHolds
	 */
	public ActiveHoldsInfoListHolder getActiveHolds() {
		return activeHolds;
	}

	/**
	 * @return the pendingHolds
	 */
	public PendingHoldsInfoListHolder getPendingHolds() {
		return pendingHolds;
	}
}
