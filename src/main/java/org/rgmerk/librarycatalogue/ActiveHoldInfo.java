package org.rgmerk.librarycatalogue;


/**
 * Stores information about an individual ActiveHold in a form suitable for 
 * JSON serialization and export through the API.
 */
public class ActiveHoldInfo {
	private long id;
	private long itemCopyId;
	private long holderId;

	public ActiveHoldInfo(ActiveHold hold) {
		this.id = hold.getId();
		this.itemCopyId = hold.getItemCopy().getId();
		this.holderId = hold.getHolder().getId();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the itemCopyId
	 */
	public long getItemCopyId() {
		return itemCopyId;
	}

	/**
	 * @param itemCopyId the itemCopyId to set
	 */
	public void setItemCopyId(long itemCopyId) {
		this.itemCopyId = itemCopyId;
	}

	/**
	 * @return the holder
	 */
	public long getHolderId() {
		return holderId;
	}

	/**
	 * @param holder the holder to set
	 */
	public void setHolderId(long holder) {
		this.holderId = holder;
	}
}
