package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A wrapper around a list of ActiveHoldInfo objects.
 * 
 * FIXME: I am not sure this is absolutely required but Jackson's ObjectMapper seems to get
 * horribly confused by generics and this seems to be the magic workaround.  
 */
public class ActiveHoldsInfoListHolder {
	private List <ActiveHoldInfo> activeHolds;
	
	public ActiveHoldsInfoListHolder() {
		activeHolds=new ArrayList<ActiveHoldInfo>();
	}
	
	@JsonCreator
	ActiveHoldsInfoListHolder(@JsonProperty("activeHolds") List<ActiveHoldInfo> activeHolds) {
		this.activeHolds=activeHolds;
	}

	/**
	 * @return the activeHolds
	 */
	public List <ActiveHoldInfo> getActiveHolds() {
		return activeHolds;
	}

	/**
	 * @param activeHolds the activeHolds to set
	 */
	public void setActiveHolds(List <ActiveHoldInfo> activeHolds) {
		this.activeHolds = activeHolds;
	}
}
