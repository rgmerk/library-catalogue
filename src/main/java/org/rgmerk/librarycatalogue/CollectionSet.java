package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Represents information about the set of ItemCopies of a (single) Book stored in a LibraryCollection
 *
 * Pending holds are stored here, because you don't care which copy from the LibraryCollection 
 * you ultimately get.  Active holds and Borrowings are associated with a specific ItemCopy in
 * the set.
 */
@Entity
public class CollectionSet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name ="fk_book")
	private Book book;
	
	@ManyToOne
	@JoinColumn(name = "fk_librarycollection")
	private LibraryCollection collection;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="collectionSet")
	List<PendingHold> pendingHolds = new ArrayList<PendingHold>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="collectionSet")
	List<ItemCopy> copies=new ArrayList<ItemCopy>();

	protected CollectionSet() {}
	
	public CollectionSet(Book book) {
		this.book = book;
	}
	
	/**
	 * Return the LibraryCollection this CollectionSet belongs to
	 */
	public LibraryCollection getCollection() {
		return collection;
	}

	/**
	 * @param Set which LibraryCollection this CollectionSet belongs to.
	 * 
	 * This should generally only be set once.  
	 */
	public void setCollection(LibraryCollection collection) {
		this.collection = collection;
	}
	
	/**
	 * return all copies in this collectionSet
	 */
	public List<ItemCopy> getCopies() {
		return copies;
	}
	
	/**
	 * add an ItemCopy to the CollectionSet
	 */
	public void addCopy(ItemCopy copy) {
		copies.add(copy);
		copy.setCollectionSet(this);
	}


	/**
	 * Return true if the borrower has an active hold on any itemcopy in this CollectionSet
	 * 
	 */	
	public boolean haveActiveHold(Borrower b) {
		return (getActiveHold(b) != null);
	}
	
	/**
	 * Return the active hold if the borrower has one on any itemcopy in this CollectionSet
	 * 
	 * Return null otherwise
	 * 
	 * FIXME: possibly refactor to use this rather than haveActiveHold
	 */	
	public ActiveHold getActiveHold(Borrower b) {
		for (ItemCopy i: copies) {
			ActiveHold activeHold = i.getActiveHold();
			if(activeHold != null && activeHold.getHolder().getId() == b.getId()) {
				return activeHold;
			}
		}
		return null;
	}
	
	/**
	 * return the first (earliest placed) pending hold on this collectionSet
	 * 
	 * Used when a book is returned so we can place an ActiveHold for the Borrower
	 * who has been waiting the longest.
	 * 
	 */
	public PendingHold getFirstPendingHold() {
		if (pendingHolds.size() == 0) {
			return null;
		}
		
		PendingHold earliest = pendingHolds.get(0);
		
		for (PendingHold p : pendingHolds) {
			if(p.getRequestDateTime().isBefore(earliest.getRequestDateTime())) {
				earliest = p;
			}
		}
		
		return earliest;
	}
	
	/**
	 * remove a pending hold on this CollectionSet.
	 * 
	 * NOTE: this doesn't actually call the update on the raw database objects.
	 */
	public void clearPendingHold(PendingHold p) {
		// find the hold
		int holdIndex = pendingHolds.indexOf(p);
		if (holdIndex<0) {
			return;
		}
		// disassociate the hold from this collectionset in both directions.
		p.setCollectionSet(null);
		p.setHolder(null);
		pendingHolds.remove(holdIndex);
		return;
		
	}
	/**
	 * return true if borrower has a pending hold in the hold queue
	 * 
	 */
	
	public boolean havePendingHold(Borrower b) {
		return (getPendingHold(b) != null);
	}

	/**
	 * if borrower has a pending hold in the hold queue, return it, else return null
	 * 
	 */
	public PendingHold getPendingHold(Borrower b) {
		for(PendingHold p: pendingHolds) {
			if(p.getHolder().getId() == b.getId()) {
				return p;
			}
		}
		
		return null;
	}

	/**
	 * return an available (non-borrowed, non-actively held) ItemCopy in this CollectionSet
	 */
	public ItemCopy getAvailableCopy() {
		for (ItemCopy i: copies) {
			if (i.isAvailable()) {
				return i;
			}
		}
		return null;
	}
	
	/**
	 * return the number of pending holds
	 */
	public int getPendingHoldCount() {
		return pendingHolds.size();
	}
	
	/**
	 * @return this CollectionSet's unique id.
	 */
	public long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return the Book associated with this CollectionSet.
	 */
	public Book getBook() {
		return book;
	}
}
