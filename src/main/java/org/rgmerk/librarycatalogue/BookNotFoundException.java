package org.rgmerk.librarycatalogue;


/**
 * Thrown by a Controller when a specified Book id does not exist in the catalogue.
 *
 */
class BookNotFoundException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;

	BookNotFoundException(Long id) {
		super("Could not find book " + id);
	}
}


