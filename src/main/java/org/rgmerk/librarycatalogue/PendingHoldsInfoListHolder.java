package org.rgmerk.librarycatalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * A wrapper around a list of PendingHoldInfo objects, used for JSON serialization.
 * 
 * FIXME: I am not sure this is absolutely required but Jackson's ObjectMapper seems to get
 * horribly confused by generics, particularly when some instances are of subclasses,
 * and this seems to be the magic workaround.  
 */
public class PendingHoldsInfoListHolder {
	private List <PendingHoldInfo> pendingHolds;
	
	public PendingHoldsInfoListHolder () {}
	
	@JsonCreator
	PendingHoldsInfoListHolder(@JsonProperty("pendingHolds") List<PendingHoldInfo> pendingHolds) {
		this.pendingHolds=pendingHolds;
	}


	public List <PendingHoldInfo> getPendingHolds() {
		return pendingHolds;
	}

	public void setPendingHolds(List <PendingHoldInfo> pendingHolds) {
		this.pendingHolds = pendingHolds;
	}
}