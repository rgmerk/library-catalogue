package org.rgmerk.librarycatalogue;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Tells the spring Framework what to do when a EntityNotFoundException subclass is thrown from a Controller.
 * 
 * You should never need to instantiate a member of this class manually.
 *
 */
@ControllerAdvice
class EntityNotFoundAdvice {

  @ResponseBody
  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String EntityNotFoundHandler(EntityNotFoundException ex) {

    return ex.getMessage();
  }
}