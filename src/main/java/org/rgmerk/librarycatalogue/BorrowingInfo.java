package org.rgmerk.librarycatalogue;

import java.time.LocalDate;

/**
 * Constains information about a Borrowing in a form suitable for Jackson serialization, 
 * and is created directly from a single Borrowing.
 *
 * Used as a return value in an endpoint in BorrowerController.
 * 
 * Should not be used for any other purpose.
 * 
 */
public class BorrowingInfo {
	private long id;
	private long borrowerId;
	private long itemcopyId;
	private long collectionsetId;
	private long bookId;
	private String borrowDate;
	private String dueDate;
	private String returnedDate;
	
	public BorrowingInfo(Borrowing borrowing) {
		this.id = borrowing.getId();
		this.borrowerId = borrowing.getBorrower().getId();
		this.itemcopyId = borrowing.getItemcopy().getId(); // Uncle Bob hates local variables
		this.collectionsetId = borrowing.getItemcopy().getCollectionSet().getId();
		this.bookId=borrowing.getItemcopy().getCollectionSet().getBook().getId();
		this.borrowDate = borrowing.getBorrowDate().toString();
		this.dueDate = borrowing.getDueDate().toString();
		LocalDate rdate = borrowing.getReturnedDate();
		
		this.returnedDate = (rdate != null) ? borrowing.getReturnedDate().toString() : "";
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the borrower_id
	 */
	public long getBorrowerId() {
		return borrowerId;
	}
	/**
	 * @param borrower_id the borrower_id to set
	 */
	public void setBorrowerId(long borrowerId) {
		this.borrowerId = borrowerId;
	}
	/**
	 * @return the itemcopyId
	 */
	public long getItemcopyId() {
		return itemcopyId;
	}
	/**
	 * @param itemcopyId the itemcopyId to set
	 */
	public void setItemcopyId(long itemcopyId) {
		this.itemcopyId = itemcopyId;
	}
	/**
	 * @return the collectionsetId
	 */
	public long getCollectionsetId() {
		return collectionsetId;
	}
	/**
	 * @param collectionsetId the collectionsetId to set
	 */
	public void setCollectionsetId(long collectionsetId) {
		this.collectionsetId = collectionsetId;
	}
	/**
	 * @return the bookId
	 */
	public long getBookId() {
		return bookId;
	}
	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(long bookId) {
		this.bookId = bookId;
	}
	/**
	 * @return the borrowDate
	 */
	public String getBorrowDate() {
		return borrowDate;
	}
	/**
	 * @param borrowDate the borrowDate to set
	 */
	public void setBorrowDate(String borrowDate) {
		this.borrowDate = borrowDate;
	}
	/**
	 * @return the dueDate
	 */
	public String getDueDate() {
		return dueDate;
	}
	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	/**
	 * @return the returnDate
	 */
	public String getReturnedDate() {
		return returnedDate;
	}
	/**
	 * @param returnDate the returnDate to set
	 */
	public void setReturnedDate(String returnDate) {
		this.returnedDate = returnDate;
	}
	
	
}
