package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.FetchType;


/**
 * Represents information about a book stored in the library.
 *
 * Individual book copies are represented as ItemCopies and are grouped in CollectionSets
 * 
 * Ideally, this would have been a subclass of something like CatalogueItem.  CDs, 
 * Online resources, book chapters, and so on would have also been subclasses of CatalogueItem.
 * 
 * A real library catalogue would of course want to store a lot more information about books.
 */
@Entity
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String title;
	private String author;
	private int yearPublished;
	
	@JsonIgnore
	@OneToMany(fetch=FetchType.EAGER, mappedBy="book", cascade=CascadeType.ALL)
	private List<CollectionSet> collectionsets = new ArrayList<CollectionSet>();
	
	protected Book() {}

	/**
	 * @param title The title of the book
	 * @param author The author of the book
	 * @param yearpublished The year the book was published 
	 */
	public Book(String title, String author, int yearpublished) {
		this.title = title;
		this.author = author;
		this.setYearPublished(yearpublished);
	}
	
	public String toString() {
		return this.title + " " + this.author;
	}

	public int getYearPublished() {
		return yearPublished;
	}

	public void setYearPublished(int yearpublished) {
		this.yearPublished = yearpublished;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public List<CollectionSet> getCollectionsets() {
		return collectionsets;
	}
}
