package org.rgmerk.librarycatalogue;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRUDRepository for LibraryCollections
 *
 * Implementations are automatically created and instantiated by Spring Boot where required.
 * 
 */
@Transactional
public interface LibraryCollectionRepository extends CrudRepository<LibraryCollection, Long> {

	List<Book> findByName(@Param("name") String name);
	LibraryCollection findById(long id);
	List<LibraryCollection> findAll();

}