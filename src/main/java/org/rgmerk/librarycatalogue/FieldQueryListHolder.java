package org.rgmerk.librarycatalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A wrapper around a list of FieldQuery objects, used for JSON serialization.
 * 
 * FIXME: I am not sure this is absolutely required but Jackson's ObjectMapper seems to get
 * horribly confused by generics, particularly when some instances are of subclasses,
 * and this seems to be the magic workaround.  
 */
class FieldQueryListHolder {

	private List<FieldQuery> queries;

	FieldQueryListHolder() {};
	
	@JsonCreator
	FieldQueryListHolder(@JsonProperty("queries") List<FieldQuery> queries) {
		this.setQueries(queries);
	}
	public List<FieldQuery> getQueries() {
		return queries;
	}

	public void setQueries(List<FieldQuery> queries) {
		this.queries = queries;
	}
	
}