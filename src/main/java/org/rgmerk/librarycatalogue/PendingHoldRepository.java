package org.rgmerk.librarycatalogue;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * CRUDRepository for LibraryCollections
 *
 * Implementations are automatically created and instantiated by Spring Boot where required.
 * 
 */
@Transactional
public interface PendingHoldRepository extends CrudRepository<PendingHold, Long> {

	PendingHold findById(long id);
	List<PendingHold> findAll();

}