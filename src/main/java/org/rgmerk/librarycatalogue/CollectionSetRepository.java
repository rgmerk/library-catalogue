package org.rgmerk.librarycatalogue;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for CollectionSets
 *
 * Implementations are automatically created and instantiated by Spring Boot where required.
 */
@Transactional
public interface CollectionSetRepository extends CrudRepository<CollectionSet, Long> {

	//List<ItemCopy> findByName(@Param("name") String name);
	CollectionSet findById(long id);
	List<CollectionSet> findAll();

}
