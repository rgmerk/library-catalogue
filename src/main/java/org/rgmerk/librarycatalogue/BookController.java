package org.rgmerk.librarycatalogue;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * RESTful endpoints for getting access to information about books in the collection.
 * 
 * NOTE: There is NO AUTHENTICATION WHATSOEVER on these endpoints.  
 *
 */
@RestController
class BookController {
	private static final Logger log = LoggerFactory.getLogger(BookController.class);

	private final BookRepository repository;
	private final BorrowerRepository borrowerRepository;

	/**
	 * You should not need to invoke this constructor manually.
	 * 
	 * Spring's infrastructure automatically creates an instance for you, including
	 * creating the repository objects.
	 * 
	 * @param repository
	 * @param borrowerRepository
	 */
	BookController (BookRepository repository, BorrowerRepository borrowerRepository) {
		this.repository = repository;
		this.borrowerRepository = borrowerRepository;
	}


	/**
	 * Information about every Book in the library.
	 * 
	 * NOTE: In a real catalogue such a method would be infeasibly slow and wouldn't be
	 * exposed.
	 */
	@GetMapping("/books")
	List<Book> all() {
		return repository.findAll();
	}

	/**
	 * Returns basic information about books matching a complex query.
	 * 
	 * The complex queries are defined by a list of subclasses of FieldQuery, which are intended
	 * to be constructed from the JSON object sent in the GET request. See the documentation
	 * for BookQuerySpecification.matchesQueryFields for more information about the query fields,
	 * and how all the Spring Data magic is used.
	 * 
	 * FIXME: this uses a POST request because of a complex query format.  This really feels like
	 * it should be a GET because it doesn't change the state of the server, but 
	 * GET request bodies are supposed to be ignored.  *shrug*
	 * 
	 * @param queries
	 * @return
	 */
	@PostMapping(path="/books/search", consumes= {"application/json", "application/json; charset=utf-8"})
	List <Book> search(@RequestBody FieldQueryListHolder queries) {
		log.info("search query received");
		return repository.findAll(BookQuerySpecification.matchesQueryFields(queries.getQueries()));
	}


	/**
	 * Basic information about a specific Book identified by its GUID
	 * @param id 
	 * @return Basic information about the identified book
	 * @throws BookNotFoundException if there is no such book.
	 */
	@GetMapping("/book/{id}")
	Book one(@PathVariable Long id) {

		return repository.findById(id)
				.orElseThrow(() -> new BookNotFoundException(id));
	}
	
	/**
	 * Detailed information about a specific book identified by its GUID
	 * 
	 * This endpoint provides information 
	 * @param id
	 * @param borrowerid
	 * @return
	 */
	@GetMapping("/book/{id}/extendedinfo/{borrowerid}")
	BookExtendedInfo oneExtended(@PathVariable Long id, @PathVariable Long borrowerid) {
		Book theBook = repository.findById(id)
		.orElseThrow(() -> new BookNotFoundException(id));
		
		Borrower theBorrower = borrowerRepository.findById(borrowerid) 
				.orElseThrow(() -> new BorrowerNotFoundException(borrowerid));
		return new BookExtendedInfo(theBook, theBorrower);
	}

}