package org.rgmerk.librarycatalogue;

import javax.persistence.criteria.Root;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;

/**
 * Abstract superclass representing a query on a specific Book field.
 * 
 * The subclasses are instantiated by deserialization of a FieldQueryListHolder by Jackson.
 * GetPredicate generates the actual Predicate based on the Query information which is the
 * combined into a Specification by BookQuerySpecification.matchQueryFields
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
public abstract class FieldQuery {
    //JsonTypeInfo header tells Jackson to include the class name as a property of
	//serialized instances of subclasses, and deserialize based on that information.
	public abstract Predicate getPredicate(Root<Book> root, CriteriaBuilder cb) 
			throws InvalidQueryException;
}
