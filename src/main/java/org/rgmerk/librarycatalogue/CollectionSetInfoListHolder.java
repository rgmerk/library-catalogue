package org.rgmerk.librarycatalogue;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A wrapper around a list of CollectionSet objects, used for JSON serialization.
 * 
 * FIXME: I am not sure this is absolutely required but Jackson's ObjectMapper seems to get
 * horribly confused by generics and this seems to be the magic workaround.  
 */
public class CollectionSetInfoListHolder {
	private List<CollectionSetInfo> collectionSetInfos;

	CollectionSetInfoListHolder() {
		collectionSetInfos=new ArrayList<CollectionSetInfo>();
	};
	
	@JsonCreator
	CollectionSetInfoListHolder(@JsonProperty("collectionSets") List<CollectionSetInfo> infos) {
		this.setCollectionSetInfos(infos);
	}
	public List<CollectionSetInfo> getCollectionSetInfos() {
		return collectionSetInfos;
	}

	public void setCollectionSetInfos(List<CollectionSetInfo> infos) {
		this.collectionSetInfos = infos;
	}
	
}
