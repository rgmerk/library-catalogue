package org.rgmerk.librarycatalogue;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * An interface for performing CRUD operations on ActiveHolds.  
 * 
 * The implementation is automagically created by Spring Data. 
 */
@Transactional
public interface ActiveHoldRepository extends CrudRepository<ActiveHold, Long> {

	ActiveHold findById(long id);
	List<ActiveHold> findAll();

}