package org.rgmerk.librarycatalogue;

/**
 * Exception thrown when a FieldQuery can't be turned into a Predicate
 *
 */
public class InvalidQueryException extends RuntimeException {
	private static final long serialVersionUID = -8412273643272170812L;
 
	InvalidQueryException() {
		//FIXME: make this message generic
		super("Date range is invalid");
	}
}
