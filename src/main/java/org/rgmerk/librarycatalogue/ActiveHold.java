package org.rgmerk.librarycatalogue;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Represents information about a specific ItemCopy currently reserved for a borrower 
 * (the "holder" on the "hold shelf".
 */
@Entity
public class ActiveHold {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@OneToOne
	@JoinColumn(name="fk_itemcopy")
	private ItemCopy itemCopy;
	
	@ManyToOne
	@JoinColumn(name="fk_holder")
	private Borrower holder;

	public ActiveHold() {}
	
	public ItemCopy getItemCopy() {
		return itemCopy;
	}

	public void setItemCopy(ItemCopy itemCopy) {
		this.itemCopy = itemCopy;
	}

	public Borrower getHolder() {
		return holder;
	}

	public void setHolder(Borrower holder) {
		this.holder = holder;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
}
