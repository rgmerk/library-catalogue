package org.rgmerk.librarycatalogue;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Represents a pending hold requested by a borrower.
 * 
 * PendingHolds are associated with a CollectionSet because we don't really care which
 * copy of a Book we get, as long as we get one with the borrowing rights we wanted.
 * 
 * When an ItemCopy is returned, the return method checks for PendingHolds in the parent
 * collectionSet and if there are any, it converts the earliest unfulfilled PendingHold
 * into an ActiveHold on the returned ItemCopy.
 *
 */
@Entity
public class PendingHold {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="fk_collectionset")
	private CollectionSet collectionSet;
	
	@ManyToOne
	@JoinColumn(name="fk_borrower")
	private Borrower holder;
	
	// Note use of a datetime rather than a date 
    @Column(name = "request_date", columnDefinition = "DATETIME")
    private LocalDateTime requestDateTime;

    protected PendingHold() {}
    
    public PendingHold(CollectionSet collectionSet, Borrower borrower) {
    	this.collectionSet = collectionSet;
    	this.holder = borrower;
    	this.requestDateTime = LocalDateTime.now();
    }
    /**
     * returns the Borrower who placed this hold.
     */
    public Borrower getHolder() {
		return holder;
	}


	/**
	 * return the requestDateTime
	 */
	public LocalDateTime getRequestDateTime() {
		return requestDateTime;
	}

	/**
	 * @param requestDateTime the requestDateTime to set
	 */
	public void setRequestDateTime(LocalDateTime requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	/**
	 * return the collectionSet
	 */
	public CollectionSet getCollectionSet() {
		return collectionSet;
	}

	/**
	 * @param collectionSet the collectionSet to set
	 */
	public void setCollectionSet(CollectionSet collectionSet) {
		this.collectionSet = collectionSet;
	}

	/**
	 * @param holder the holder to set
	 */
	public void setHolder(Borrower holder) {
		this.holder = holder;
	}

	/**
	 * return the id
	 */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
